<?php


/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */

use Illuminate\Support\Facades\Hash;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'password' =>  Hash::make('secret')
    ];
});
