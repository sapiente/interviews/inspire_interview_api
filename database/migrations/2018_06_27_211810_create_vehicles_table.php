<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('weight')->comment('[kg]');
            $table->unsignedInteger('performance')->comment('[W]');
            $table->double('daily_price')->comment('[$]');
            $table->string('driving_licence_group_id', 5);
            $table->unsignedInteger('owner_id');
            $table->unsignedInteger('generalized_id');
            $table->string('generalized_type');
            $table->timestamps();

            $table->foreign('driving_licence_group_id')->references('id')->on('driving_licence_groups');
            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
