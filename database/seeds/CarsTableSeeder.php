<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $car = \App\Models\Car::create(['driving_range' => 10]);
        $vehicle = new \App\Models\Vehicle([
            'weight' => 80,
            'performance' => 10,
            'daily_price' => 20,
        ]);
        $vehicle->owner()->associate(1);
        $vehicle->drivingLicenceGroup()->associate('A');
        $car->vehicle()->save($vehicle);
    }
}
