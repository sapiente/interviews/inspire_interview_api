<?php

use Illuminate\Database\Seeder;

class DrivingLicenceGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\DrivingLicenceGroup::unguard();
        collect([
            ['id' => 'AM'],
            ['id' => 'A1'],
            ['id' => 'A2'],
            ['id' => 'A'],
            ['id' => 'B1'],
            ['id' => 'B'],
            ['id' => 'B+E'],
            ['id' => 'C1'],
            ['id' => 'C1+E'],
            ['id' => 'C'],
            ['id' => 'C+E'],
            ['id' => 'D1'],
            ['id' => 'D1+E'],
            ['id' => 'D'],
            ['id' => 'D+E'],
            ['id' => 'T'],
        ])->each(function($row) {
            \App\Models\DrivingLicenceGroup::create($row);
        });
    }
}
