<?php

use App\Models\MotorcycleType;
use Illuminate\Database\Seeder;

class MotorcycleTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MotorcycleType::unguard();
        collect([
            ['name' => 'Standard'],
            ['name' => 'Cruiser'],
            ['name' => 'Touring'],
            ['name' => 'Sport touring'],
        ])->each(function($row) {
            MotorcycleType::create($row);
        });
    }
}
