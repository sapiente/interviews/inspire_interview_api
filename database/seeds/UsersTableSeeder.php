<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::create([
            'email' => 'inspire@inspire.cz',
            'password' => Hash::make('secret')
        ]);

        factory(App\Models\User::class, 20)->create();
    }
}
