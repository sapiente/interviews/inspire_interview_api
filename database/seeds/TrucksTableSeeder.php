<?php

use Illuminate\Database\Seeder;

class TrucksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $truck = \App\Models\Truck::create(['loading_area' => 10, 'max_weight' => 1]);
        $vehicle = new \App\Models\Vehicle([
            'weight' => 80,
            'performance' => 10,
            'daily_price' => 20,
        ]);
        $vehicle->owner()->associate(1);
        $vehicle->drivingLicenceGroup()->associate('A');
        $truck->vehicle()->save($vehicle);
    }
}
