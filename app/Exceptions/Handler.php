<?php

namespace App\Exceptions;

use App\Utils\ResponseFactory;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class Handler extends ExceptionHandler
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof HttpResponseException || $e instanceof ValidationException && $e->getResponse()) {
            return $e->getResponse();
        }

        $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        $message = (env('APP_DEBUG', false) ? $e->getMessage() : null) ?: JsonResponse::$statusTexts[$statusCode];
        $headers = $e instanceof HttpExceptionInterface ? $e->getHeaders() : [];

        return ResponseFactory::error($message, $statusCode)->withHeaders($headers);
    }
}
