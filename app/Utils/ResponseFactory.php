<?php


namespace App\Utils;
use Illuminate\Http\JsonResponse;


/**
 * Response factory using JSend (@see https://labs.omniti.com/labs/jsend) standard.
 */
class ResponseFactory
{
    private const
        SUCCESS = 'success',
        FAIL = 'fail',
        ERROR = 'error';

    public static function created($data = null)
    {
        return self::success($data, JsonResponse::HTTP_CREATED);
    }

    public static function success($data = null, int $status = JsonResponse::HTTP_OK)
    {
        return self::from($status, self::envelope(self::SUCCESS, null, $data));
    }

    public static function fail($data, int $status = JsonResponse::HTTP_BAD_REQUEST)
    {
        return self::from($status, self::envelope(self::FAIL, null, $data));
    }

    public static function error(string $message, int $status = JsonResponse::HTTP_INTERNAL_SERVER_ERROR)
    {
        return self::from($status, self::envelope(self::ERROR, $message, null));
    }

    /**
     * @param int $status
     * @param array $envelope
     * @return JsonResponse
     */
    private static function from(int $status, array $envelope)
    {
        return JsonResponse::create($envelope, $status);
    }

    /**
     * @param string $status
     * @param null|string $message
     * @param mixed $data
     * @return array
     */
    private static function envelope(string $status, ?string $message, $data)
    {
        return array_filter(compact('status', 'message', 'data'));
    }
}