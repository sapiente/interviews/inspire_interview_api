<?php


namespace App\Helpers;


class JWT
{
    public static function decode(?string $jwt)
    {
        return \Firebase\JWT\JWT::decode($jwt, env("APP_KEY"), ['HS512', 'HS256']);
    }

    public static function encode(array $payload)
    {
        return \Firebase\JWT\JWT::encode(array_merge($payload, ['exp' => time() + env('JWT_EXPIRATION')]), env("APP_KEY"), env('JWT_ALG', 'HS512'));
    }
}