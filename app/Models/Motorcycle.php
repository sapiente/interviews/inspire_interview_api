<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Motorcycle extends Model
{
    protected $with = ['type'];

    public function type()
    {
        return $this->belongsTo(MotorcycleType::class);
    }

    public function vehicle()
    {
        return $this->morphOne(Vehicle::class, 'generalized');
    }
}
