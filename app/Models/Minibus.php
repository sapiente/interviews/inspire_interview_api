<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Minibus extends Model
{
    protected $fillable = ['seats'];

    public function vehicle()
    {
        return $this->morphOne(Vehicle::class, 'generalized');
    }
}
