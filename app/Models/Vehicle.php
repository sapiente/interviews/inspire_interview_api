<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = ['weight', 'performance', 'daily_price'];

    public function generalized()
    {
        return $this->morphTo();
    }

    public function drivingLicenceGroup()
    {
        return $this->belongsTo(DrivingLicenceGroup::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}
