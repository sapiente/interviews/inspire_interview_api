<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DrivingLicenceGroup extends Model
{
    protected $keyType = 'string';

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }
}
