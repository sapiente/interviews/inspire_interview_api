<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['driving_range'];

    public function vehicle()
    {
        return $this->morphOne(Vehicle::class, 'generalized');
    }
}
