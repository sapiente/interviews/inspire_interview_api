<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    protected $fillable = ['loading_area', 'max_weight'];

    public function vehicle()
    {
        return $this->morphOne(Vehicle::class, 'generalized');
    }
}
