<?php


namespace App\Http\Controllers;


use App\Http\Responses\VehicleResponse;
use App\Models\Vehicle;
use App\Services\VehicleService;
use App\Utils\ResponseFactory;
use Illuminate\Support\Collection;

class VehicleController extends Controller
{
    const VEHICLE_REGISTRATION_VALIDATIONS = [
        'weight'                => 'required|integer',
        'performance'           => 'required|integer',
        'daily_price'           => 'required|numeric',
        'driving_licence_group_id' => 'required|exists:driving_licence_groups,id',
    ];

    /** @var VehicleService */
    private $service;

    /**
     * VehicleController constructor.
     * @param VehicleService $service
     */
    public function __construct(VehicleService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function list()
    {
        $data = Vehicle::with(['generalized', 'owner'])
            ->get()
            ->groupBy('generalized_type')
            ->mapWithKeys(function (Collection $group, $key) {
                $type = strtolower(last(explode('\\', $key)));
                $values     = $group->map(function(Vehicle $vehicle) {
                    return VehicleResponse::from($vehicle);
                });

                return [str_plural($type) => $values];
            });

        return ResponseFactory::success($data);
    }

    public function delete(int $vehicle)
    {
        $this->service->delete($vehicle);

        return ResponseFactory::success();
    }
}