<?php


namespace App\Http\Controllers;


use App\Models\DrivingLicenceGroup;
use App\Utils\ResponseFactory;

class DrivingLicenceController extends Controller
{

    public function listGroups()
    {
        return ResponseFactory::success(['groups' => DrivingLicenceGroup::all('id')]);
    }
}