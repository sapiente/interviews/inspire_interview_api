<?php


namespace App\Http\Controllers;


use App\Http\Responses\VehicleResponse;
use App\Services\MinibusService;
use App\Utils\ResponseFactory;
use Illuminate\Http\Request;

class MinibusController extends Controller
{
    /** @var MinibusService */
    private $service;

    /**
     * CarController constructor.
     * @param MinibusService $service
     */
    public function __construct(MinibusService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function register(Request $request)
    {
        $validated = $this->validate($request, array_merge( VehicleController::VEHICLE_REGISTRATION_VALIDATIONS, [
            'seats' => 'required|integer',
        ]));

        $vehicle = $this->service->register($validated);

        return ResponseFactory::created(['vehicle' => VehicleResponse::from($vehicle)]);
    }

    public function update(int $id, Request $request)
    {
        $validated = $this->validate($request, array_merge(VehicleController::VEHICLE_REGISTRATION_VALIDATIONS, [
            'seats' => 'required|integer',
        ]));

        $vehicle = $this->service->update($id, $validated);

        return ResponseFactory::success(['vehicle' => VehicleResponse::from($vehicle)]);
    }
}