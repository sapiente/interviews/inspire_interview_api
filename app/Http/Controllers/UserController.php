<?php

namespace App\Http\Controllers;

use App\Utils\ResponseFactory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function identity()
    {
        return ResponseFactory::success([
            'user' => Arr::only(Auth::user()->toArray(), ['id', 'email'])
        ]);
    }
}
