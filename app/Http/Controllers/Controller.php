<?php

namespace App\Http\Controllers;

use App\Utils\ResponseFactory;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        self::buildResponseUsing(function(Request $request, array $errors) {
            return ResponseFactory::fail($errors);
        });
    }
}
