<?php

namespace App\Http\Controllers;

use App\Helpers\JWT;
use App\Services\UserService;
use App\Utils\ResponseFactory;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /** @var UserService */
    private $userService;

    /**
     * AuthController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        parent::__construct();
        $this->userService = $service;
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
            'email'    => 'required|email'
        ]);

        $user = $this->userService->login($request->input('email'), $request->input('password'));

        return ResponseFactory::success([
            'token' => JWT::encode(['id' => $user->id, 'email' => $user->email]),
        ]);
    }
}
