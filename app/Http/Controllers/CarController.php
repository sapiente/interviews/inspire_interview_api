<?php


namespace App\Http\Controllers;


use App\Http\Responses\VehicleResponse;
use App\Services\CarService;
use App\Utils\ResponseFactory;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /** @var CarService */
    private $service;

    /**
     * CarController constructor.
     * @param CarService $service
     */
    public function __construct(CarService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function register(Request $request)
    {
        $validated = $this->validate($request, array_merge( VehicleController::VEHICLE_REGISTRATION_VALIDATIONS, [
            'driving_range' => 'required|integer',
        ]));

        $vehicle = $this->service->register($validated);

        return ResponseFactory::created(['vehicle' => VehicleResponse::from($vehicle)]);
    }

    public function update(int $id, Request $request)
    {
        $validated = $this->validate($request, array_merge(VehicleController::VEHICLE_REGISTRATION_VALIDATIONS, [
            'driving_range' => 'required|integer',
        ]));

        $vehicle = $this->service->update($id, $validated);

        return ResponseFactory::success(['vehicle' => VehicleResponse::from($vehicle)]);
    }
}