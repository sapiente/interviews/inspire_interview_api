<?php


namespace App\Http\Controllers;


use App\Http\Responses\VehicleResponse;
use App\Models\MotorcycleType;
use App\Services\MotorcycleService;
use App\Utils\ResponseFactory;
use Illuminate\Http\Request;

class MotorcycleController extends Controller
{
    /** @var MotorcycleService */
    private $service;

    /**
     * MotorcycleController constructor.
     * @param MotorcycleService $service
     */
    public function __construct(MotorcycleService $service)
    {
        parent::__construct();
        $this->service = $service;
    }


    public function register(Request $request)
    {
        $validated = $this->validate($request, array_merge(VehicleController::VEHICLE_REGISTRATION_VALIDATIONS, [
            'type' => 'required|exists:motorcycle_types,id',
        ]));

        $vehicle = $this->service->register($validated);

        return ResponseFactory::created(['vehicle' => VehicleResponse::from($vehicle)]);
    }

    public function update(int $id, Request $request)
    {
        $validated = $this->validate($request, array_merge(VehicleController::VEHICLE_REGISTRATION_VALIDATIONS, [
            'type' => 'required|exists:motorcycle_types,id',
        ]));

        $vehicle = $this->service->update($id, $validated);

        return ResponseFactory::success(['vehicle' => VehicleResponse::from($vehicle)]);
    }

    public function listTypes()
    {
        return ResponseFactory::success(['types' => MotorcycleType::all(['id', 'name'])]);
    }
}