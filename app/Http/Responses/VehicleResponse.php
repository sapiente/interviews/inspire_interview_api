<?php


namespace App\Http\Responses;


use App\Models\Vehicle;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class VehicleResponse extends Collection
{
    public static function from(Vehicle $vehicle)
    {
        $attributes = $vehicle->toArray();
        return new self(array_merge(
            Arr::only($attributes, ['id', 'weight', 'performance', 'daily_price', 'driving_licence_group_id']),
            [
                'type'        => strtolower(last(explode('\\', $vehicle['generalized_type']))),
                'generalized' => Arr::except($attributes['generalized'], 'updated_at'),
                'owner'       => Arr::only($attributes['owner'], ['id', 'email'])
            ]
        ));
    }
}