<?php


namespace App\Services;


use App\Models\Minibus;
use App\Models\Vehicle;

class MinibusService
{
    /** @var VehicleService */
    private $vehicleService;

    /**
     * CarsService constructor.
     * @param VehicleService $vehicleService
     */
    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }


    public function register(array $data) : Vehicle
    {
        return $this->vehicleService->create(Minibus::create($data), $data);
    }


    public function update(int $id, array $data) : Vehicle
    {
        /** @var Minibus $minibus */
        $minibus = Minibus::findOrFail($id);
        $minibus->update($data);

        return $this->vehicleService->update($minibus->vehicle, $data);
    }
}