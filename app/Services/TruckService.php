<?php


namespace App\Services;



use App\Models\Truck;
use App\Models\Vehicle;

class TruckService
{
    /** @var VehicleService */
    private $vehicleService;

    /**
     * CarsService constructor.
     * @param VehicleService $vehicleService
     */
    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }


    public function register(array $data) : Vehicle
    {
        return $this->vehicleService->reateVehicle(Truck::create($data), $data);
    }


    public function update(int $id, array $data) : Vehicle
    {
        /** @var Truck $truck */
        $truck = Truck::findOrFail($id);
        $truck->update($data);

        return $this->vehicleService->update($truck->vehicle, $data);
    }
}