<?php


namespace App\Services;


use App\Models\Motorcycle;
use App\Models\Vehicle;

class MotorcycleService
{
    /** @var VehicleService */
    private $vehicleService;

    /**
     * CarsService constructor.
     * @param VehicleService $vehicleService
     */
    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }

    public function register(array $data) : Vehicle
    {
        $motorcycle = new Motorcycle();
        $motorcycle->type()->associate($data['type']);
        $motorcycle->save();

        return $this->vehicleService->create($motorcycle, $data);
    }

    public function update(int $id, array $data) : Vehicle
    {
        /** @var Motorcycle $motorcycle */
        $motorcycle = Motorcycle::findOrFail($id);
        $motorcycle->type()->associate($data['type']);
        $motorcycle->save();

        return $this->vehicleService->update($motorcycle->vehicle, $data);
    }
}