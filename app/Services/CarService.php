<?php


namespace App\Services;


use App\Models\Car;
use App\Models\Vehicle;

class CarService
{
    /** @var VehicleService */
    private $vehicleService;

    /**
     * CarsService constructor.
     * @param VehicleService $vehicleService
     */
    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }


    public function register(array $data) : Vehicle
    {
        return $this->vehicleService->create(Car::create($data), $data);
    }


    public function update(int $id, array $data) : Vehicle
    {
        /** @var Car $car */
        $car = Car::findOrFail($id);
        $car->update($data);

        return $this->vehicleService->update($car->vehicle, $data);
    }
}