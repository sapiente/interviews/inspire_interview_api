<?php


namespace App\Services;


use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class VehicleService
{
    public function delete(int $vehicle)
    {
        /** @var Vehicle $vehicle */
        $vehicle = Vehicle::findOrFail($vehicle);
        $vehicle->generalized()->delete();
        $vehicle->delete();
    }

    public function create(Model $generalized, $data) : Vehicle
    {
        $vehicle = new \App\Models\Vehicle($data);
        $vehicle->owner()->associate(Auth::user());
        $vehicle->drivingLicenceGroup()->associate($data['driving_licence_group_id']);
        $vehicle->generalized()->associate($generalized);
        $vehicle->save();

        return $vehicle;
    }

    public function update(Vehicle $vehicle, $data) : Vehicle
    {
        $vehicle->fill($data);
        $vehicle->drivingLicenceGroup()->associate($data['driving_licence_group_id']);
        $vehicle->save();
        $vehicle->load(['generalized', 'owner']);

        return $vehicle;
    }
}