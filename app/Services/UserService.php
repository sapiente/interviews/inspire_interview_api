<?php


namespace App\Services;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserService
{
    public function login(string $email, string $password) : Authenticatable
    {
        if(! Auth::attempt(compact('email', 'password'))) {
            abort(JsonResponse::HTTP_UNAUTHORIZED, JsonResponse::$statusTexts[JsonResponse::HTTP_UNAUTHORIZED]);
        }

        return Auth::user();
    }
}