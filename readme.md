# Inspire interview API

## Requirements
1. php 7.2
    1. pdo_mysql
    1. json
1. composer
1. mysql 5.7

## Installation
1. `git clone git@gitlab.com:slythe/inspire-interview-api.git`
1. `cd inspire-interview-api`
1. `composer install`
1. `cp .env.example .env` (configure)
1. `composer ide-helper` (needs database connection configured in **.env** file)

## Run server
1. `docker-compose up -d` (or `docker-compose start` after first run)
1. `docker-compose logs -f` (show container logs)
1. `docker-compose exec api bash` (to access api container)


## Migration
1. `php artisan migrate:refresh` (needs database connection configured in **.env** file)
1. `php artisan db:seed`

## Testing

1. `vendor/bin/phpstan analyse app`