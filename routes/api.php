<?php


/** @var $router \Laravel\Lumen\Routing\Router */



$router->group(['prefix' => '/api'], function() use ($router) {
    $router->get('/', function () use ($router) {
        return $router->app->version();
    });

    $router->post('/auth/login/', 'AuthController@login');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/users/me/', 'UserController@identity');

        $router->get('/vehicles/', 'VehicleController@list');
        $router->delete('/vehicles/{id:\d+}', 'VehicleController@delete');

        $router->post('/cars/', 'CarController@register');
        $router->put('/cars/{id:\d+}', 'CarController@update');

        $router->post('/trucks/', 'TruckController@register');
        $router->put('/trucks/{id:\d+}', 'TruckController@update');

        $router->post('/minibuses/', 'MinibusController@register');
        $router->put('/minibuses/{id:\d+}', 'MinibusController@update');

        $router->post('/motorcycles/', 'MotorcycleController@register');
        $router->put('/motorcycles/{id:\d+}', 'MotorcycleController@update');
        $router->get('/motorcycles/types/', 'MotorcycleController@listTypes');


        $router->get('/driving-licences/groups/', 'DrivingLicenceController@listGroups');
    });
});

